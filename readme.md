Xenforo's [Threadmark Addon](https://xenforo.com/community/resources/threadmarks.3939) is a little addon that lets OPs of a given thread tag any post within that thread, adding it to a thread-level index for easy access - of course, this is fantastic for things like stories or quests!

But if you've already got a *lot* of posts and an index, then you've got a problem, because tagging a few dozen posts by hand is no fun at all.

This tool is intended to help with that - takes a list of post IDs or URLs and automates the tagging process.

### Usage instructions:
- After downloading the file, double click it. It'll unzip - it's a self unzipping exe.
- Within it, you will find a target.txt. Open it for editing.
- It should look like this: 
>`http://forums.sufficientvelocity.com`
>
>`[USERNAME]`
>
>`[PASSWORD]`
>
>`[POST_ID], [THREADMARK_TITLE]`
>
>`[POST_ID], [THREADMARK_TITLE]`
>
>`[POST_ID], [THREADMARK_TITLE]`
- Fill in the appropriate forum URL
- Fill in your username and password on separate lines as indicated - unfortunately, this *is* necessary, since threadmarking is tied to accounts.
- Now for the exciting part! You need to fill in the threadmarks. I presume you already have some form of index, and most of that is still applicable. Any of the following formats are acceptable:
>`3356, Test one`
>
>`http://forums.sufficientvelocity.com/threads/vote-tally-program.199/#post-3400, Test two`
>
>`http://forums.sufficientvelocity.com/posts/4564/, Test three`
>
>`[url=http://forums.sufficientvelocity.com/posts/5285/]Test four[/url]`
>
>`[post=9920]Test, five[/post]`
>
>`[url=http://forums.sufficientvelocity.com/threads/vote-tally-program.199/page-2#post-22483]Test six[/url]`
- I would recommend copious use of replace to help you format the file!
- *Don't* leave an empty line at the end of the file! A complete example is, for example:
>`http://forums.sufficientvelocity.com`
>
>`Firnagzen`
>
>`[NOPE]`
>
>`3356, Test one`
>
>`http://forums.sufficientvelocity.com/threads/vote-tally-program.199/#post-3400, Test two`
>
>`http://forums.sufficientvelocity.com/posts/4564/, Test three`
>
>`[url=http://forums.sufficientvelocity.com/posts/5285/]Test four[/url]`
>
>`[post=9920]Test, five[/post]`
>
>`[url=http://forums.sufficientvelocity.com/threads/vote-tally-program.199/page-2#post-22483]Test six[/url]`
- Save that, and run Threadmark.exe, inside the same folder. The program will log you in, and run through the list.
- That's it!
### Deleting threadmarks:
- To *delete* threadmarks, you can append ', del' to the end of any of the specification lines, for example,>3356, Test one, del
>`[post=9920]Test, five[/post], del`
- You still need to specify a 'title', but that gets ignored, so it can be gibberish.