import re, sys, time, logging, requests


class XenforoBot(object):
    def __init__(self, url):
        self.url_base = url if url.endswith('/') else url + '/'
        self.conn_list = []
        self.session = requests.Session()
        self.session.headers.update({'User-Agent' : 'Firnbot'})
        logging.basicConfig(filename='log.txt', level=logging.INFO)

        self.token_re = re.compile('<input '
            'type="hidden" name="_xfToken" value="([^"]*)" ?/>'
            )

        self.tm_sep_re = re.compile(
            '(?:{0}(?:posts/|threads/[^#]*#post-)|^)'
            '([0-9]*)/? *, *(.+)'.format(re.escape(self.url_base),
            flags=re.IGNORECASE)
            )

        self.tm_tog_re = re.compile(
            '\[(?:url={0}(?:posts/|threads/[^#]*#post-)|post=)'
            '([0-9]*)/?\](.*)\[/(?:url|post)\]'.format(re.escape(self.url_base),
            flags=re.IGNORECASE)
            )

        self.tm_del_re = re.compile(' *, *del', flags = re.IGNORECASE)


    def log(self, log_str):
        print(log_str)
        logging.info(log_str)


    def check_conn(self):
        self.conn_list.append(time.time())
        try:
            if time.time() - self.conn_list[-10] < 1:
                self.log('Maximum connections over time exceeded, pausing.')
                time.sleep(1)
            self.conn_list[-10:]
        except IndexError:
            pass


    def post_retry(self, url, data):
        for i in range(3):
            self.check_conn()
            try:
                r = self.session.post(url=url, data=data)
            except requests.exceptions.RequestException as e:
                trun_dat = {i:j for i, j in data.items() if i is not 'password'}
                log_string = 'Attempt {3} posting to {0} with {1} failed:\n{2}'
                self.log(log_string.format(url, trun_dat, e, i+1))
            else:
                return r

        self.log('Sleeping for ten seconds before retrying!')
        time.sleep(10)

        for i in range(3):
            self.check_conn()
            try:
                r = self.session.post(url=url, data=data)
            except requests.exceptions.RequestException as e:
                trun_dat = {i:j for i, j in data.items() if i is not 'password'}
                log_string = 'Attempt {3} posting to {0} with {1} failed:\n{2}'
                self.log(log_string.format(url, trun_dat, e, i+4))
            else:
                return r

        self.log('Failed to connect! Exiting...')


    def login(self, username, password):
        login_url = self.url_base + 'login/login'
        print(login_url)
        login_data = {
            'login': username,
            'register': 0,
            'password': password,
            'remember': 1,
            'cookie_check': 0,
            'redirect': '/',
            '_xfToken': ''
        }
        self.r = self.post_retry(login_url, login_data)
        # print(self.r)
        self.log("Logged in to {0} as {1}!".format(forum_url, username))
        # with open('test.html', 'w') as f:
        #     f.write(self.r.text)
        self.xftoken = self.token_re.search(self.r.text).groups()[0]


    def private_message(self, pm_id, message):
        rel_res = '{0}conversations/{1}'.format(self.url_base, pm_id)
        post_url = rel_res + '/insert-reply'
        pm_data = {
            "_xfNoRedirect" : 1,
            "_xfRelativeResolver" : rel_res,
            "_xfRequestUri" : '/conversations/' + pm_id,
            "_xfToken" : self.xftoken,
            "message" : message
        }
        self.r = self.post_retry(post_url, pm_data)


    def thread_mark(self, post_id, name, delete=False):
        post_url = '{0}posts/{1}/threadmark'.format(self.url_base, post_id)
        tm_data = {
            "_xfNoRedirect" : 1,
            # "_xfRequestUri" : '/conversations/' + pm_id,
            "_xfToken" : self.xftoken,
            "label" : name,
            "_xfConfirm" : '1' if delete else '0'
        }
        # print(tm_data)
        self.r = self.post_retry(post_url, tm_data)
        # print(self.r)
        if delete:
            self.log('Deleted threadmark on post {0}!'.format(post_id))
        else:
            self.log('Threadmarked post {0} with "{1}"'.format(post_id,tm_name))



    def line_parser(self, line):
        delete = self.tm_del_re.search(line)
        if delete:
            line = line[:delete.start()]

        try:
            post_id, tm_name = self.tm_sep_re.search(line).groups()
        except AttributeError:
            post_id, tm_name = self.tm_tog_re.search(line).groups()

        return post_id, tm_name, delete



if __name__ == "__main__":
    if getattr(sys, 'frozen', False):
        # frozen
        import os
        self_dir = os.path.dirname(sys.executable)
        os.environ["REQUESTS_CA_BUNDLE"] = os.path.join(self_dir, "cacert.pem")

    else:
        pass # unfrozen
        
    try:
        with open('target.txt', 'r') as f:
            forum_url = f.readline().strip()
            username, password = f.readline().strip(), f.readline().strip()

            bot = XenforoBot(forum_url)
            bot.login(username, password)

            for i in f:
                post_id, tm_name, delete = bot.line_parser(i)
                bot.thread_mark(post_id, tm_name, delete)

        input('Complete!')

    except FileNotFoundError:
        print(
            'Target file not found! Please create a file in the format\n'
            'https://www.sufficientvelocity.com\n'
            'USERNAME\n'
            'PASSWORD\n'
            'POST_ID, THREADMARK_TITLE\n'
            'POST_ID, THREADMARK_TITLE\n'
            '...'
        )
