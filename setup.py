import sys
import requests.certs
from cx_Freeze import setup, Executable

setup(
    name = "XF Threadmarker",
    version = "0.3",
    description = "Automated bulk XF threadmarking",
    author = 'Firnagzen',
    executables = [
        Executable("threadmarker.py", base = "Console")
    ],
    options = {"build_exe":{
        "include_files":[requests.certs.where()]
    }}
)